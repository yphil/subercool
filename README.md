# SuberCool

[![pipeline status](https://framagit.org/yphil/subercool/badges/master/pipeline.svg)](https://framagit.org/yphil/subercool/-/pipelines)
[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
[![Liberapay](https://img.shields.io/liberapay/receives/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Liberapay](https://img.shields.io/liberapay/goal/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?label=buy+me+a&style=flat&logo=ko-fi&logoColor=white)](https://ko-fi.com/yphil/tiers)

Download subtitles in any language from the Gnome "Files" manager (Nautilus) or directly from the shell. 
![subercool-01](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-01.jpg)

## Installation
```shell
sudo apt install python3 exiftool
pip install requests zenipy 
mkdir -p ~/.local/bin
wget https://framagit.org/yphil/subercool/-/raw/master/subercool.py -O ~/.local/bin/subercool.py
```

### Nautilus plugin
Then to install it as a [Nautilus](https://apps.gnome.org/app/org.gnome.Nautilus/) plugin:
```shell
mkdir -p ~/.local/share/nautilus/scripts
ln -s ~/.local/bin/subercool.py ~/.local/share/nautilus/scripts/Download__Subtitles
mkdir -p ~/.config/nautilus
echo "<Control><Shift>s Download__Subtitles" >> ~/.config/nautilus/scripts-accels
```

![subercool-02](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-02.png)

## Configuration
Follow [the instructions](https://opensubtitles.stoplight.io/docs/opensubtitles-api/) to get an api key (it's quite simple really) which is a simple string of 32 characters.
Then, either pass this string as an argument like this:

```shell
subercool.py -k API_KEY ~/Videos/MyMovie.mp4
```

Or (simpler, and mandatory for the Nautilus plugin mode) put in in a file named `~/.local_variables.json` like this:

```json ~/.local_variables.json
{
    "opensubtitles_api_key": "API_KEY"
}

```

## Usage

```shell subercool.py -h
# subercool.py -h
usage: subercool.py [-h] [-k [API_KEY]] file

Download video subtitle(s)

positional arguments:
  file                  File to search in the opensubtitles database

options:
  -h, --help            show this help message and exit
  -k [API_KEY], --api_key [API_KEY]
                        API key to the opensubtitles database

See full documentation : https://framagit.org/yphil/subercool/
```
![subercool-03.png](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-03.png)

### [Commit history](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/subercool.py).

Brought to you by yPhil ; Please (please?) consider [helping](https://liberapay.com/yPhil/donate) ❤️
