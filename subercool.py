#!/usr/bin/env python3

import os
import sys
import subprocess
import argparse
import datetime
import json
# from zenipy import error, zlist, question, message, file_selection
import requests
import urllib.request

import zenipy
from types import SimpleNamespace

gui = False
home_directory = os.path.expanduser('~')
now = datetime.datetime.now().strftime('%B-%d-%H:%M:%S')

if os.path.exists(home_directory + '/.local_variables.json'):

    file = open(home_directory + '/.local_variables.json')
    local_variables = json.loads(file.read())
    if local_variables['opensubtitles_api_key']:
        opensubtitles_api_key = local_variables['opensubtitles_api_key']
    if local_variables['opensubtitles_languages']:
        opensubtitles_languages = local_variables['opensubtitles_languages']
    else:
        opensubtitles_languages = "en"

try:
    path = os.environ['NAUTILUS_SCRIPT_SELECTED_FILE_PATHS'].splitlines()[0]
    gui = True
except KeyError:

    parser = argparse.ArgumentParser(
        description='Download video subtitle(s)',
        epilog='See full documentation : https://framagit.org/yphil/subercool/')

    parser.add_argument('file',
                        help='File to search in the opensubtitles database')

    parser.add_argument('-k',
                        '--api_key',
                        nargs='?',
                        help='API key to the opensubtitles database')

    args = parser.parse_args()

    if args.file:
        path = args.file

    if args.api_key:
        opensubtitles_api_key = args.api_key

print("Searching...")

dirname = os.path.dirname(path)

search_url = "https://api.opensubtitles.com/api/v1/subtitles"

movie_name = subprocess.check_output(["exiftool -title -s -s -s '" + path + "'"], shell=True)

my_filename_sans_ext = os.path.splitext(os.path.basename(path))[0]

if not movie_name:
    movie_name = my_filename_sans_ext.replace(".", " ")

querystring = {"query": movie_name, "languages": opensubtitles_languages}

headers = {
    "Content-Type": "application/json",
    "Api-Key": opensubtitles_api_key
}

response = requests.request("GET", search_url, headers=headers, params=querystring)

names_and_ids_and_langs = []
names_and_ids_and_langs_shell = {}

x = json.loads(response.text, object_hook=lambda d: SimpleNamespace(**d))
nb_found = 0

try:
    for i in x.data:
        sub = i.attributes.files[0]
        lang = i.attributes.language
        if sub.file_id is not None and sub.file_name is not None:
            nb_found += 1
            names_and_ids_and_langs.append("{0}".format(sub.file_name))
            names_and_ids_and_langs.append("{0}".format(lang))
            names_and_ids_and_langs.append("{0}".format(sub.file_id))
            names_and_ids_and_langs_shell[sub.file_id] = [lang + " - " + sub.file_name]
except AttributeError:
    pass

beg = '\n\nSee <a href="https://framagit.org/yphil/subercool/">the documentation</a> for details.\nPlease <a href="https://liberapay.com/yPhil/">help me</a> help you ❤️'

if nb_found == 0:
    sorry = 'No subtitles found for ' + os.path.basename(path)
    if gui:
        zenipy.error(title=sorry, text=beg, width=330, height=120, timeout=None)
    sys.exit(sorry)
else:
    brag = "😎 SuberCool found %s subtitle files" % (nb_found)

if gui:
    id_to_download = zenipy.zlist(["Name", "Language", "Id"], names_and_ids_and_langs, print_columns=2, text='Select the file to download', title=brag, width=600, height=500, timeout=None)
    id_to_download = id_to_download[0]
else:
    print(brag)

    i = 0
    indexed_list_of_ids = {}
    indexed_list_of_langs = {}
    for file_id, file_name in names_and_ids_and_langs_shell.items():
        i += 1
        print("(%s) \t %s" % (i, file_name[0]))
        indexed_list_of_ids[i] = file_id
        indexed_list_of_langs[i] = file_name[0].split(' ')[0]

    print('-----------')

    while True:
        file_index = input('Enter subtitle number: ')
        try:
            file_index = int(file_index)
            id_to_download = list(indexed_list_of_ids.values())[int(file_index) - 1]
            lang_of_the_file = list(indexed_list_of_langs.values())[int(file_index) - 1]
            break
        except ValueError:
            pass
        except IndexError:
            pass

url = "https://api.opensubtitles.com/api/v1/download"

payload = {"file_id": id_to_download}
headers = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0",
    "Api-Key": opensubtitles_api_key
}

res = requests.post(url, json=payload, headers=headers, timeout=500)
result = json.loads(res.text)
download_link = str(result['link'])

req = urllib.request.Request(download_link)
req.add_header('User-Agent', 'Mozilla/5.0')
r = urllib.request.urlopen(req)
data = r.read()

subtitle_string = str(data.decode())
sub_local_file_path = '{0}/{1}.srt'.format(dirname, my_filename_sans_ext)

print('-----------')

sub_local_file = sub_local_file_path

try:
    with open(sub_local_file, 'x') as f:
        f.write(subtitle_string)
    if gui:
        zenipy.message(title='Success', text='😎 File saved to {0}.srt'.format(sub_local_file) + beg, width=330, height=120, timeout=None)

except FileExistsError:
    if not gui:
        print('File "{0}" exists, what should we do?'.format(sub_local_file_path))
        print('1 - Overwrite {0}'.format(sub_local_file_path))
        print('2 - Save to {0}/{1}-{2}.srt'.format(dirname, my_filename_sans_ext, lang_of_the_file))
        print('3 - Save to {0}/{1}-{2}.srt'.format(dirname, my_filename_sans_ext, now))
        print('4 - Choose a file name to save to')
        print('5 - Quit')
        print('-----------')

        while True:
            file_index = input('Enter a number from 1 to 5: ')
            try:
                choice_index = int(file_index)
                choice_index < 5
                break
            except ValueError:
                pass
            except IndexError:
                pass

        print('-----------')

        if choice_index == 1:
            sub_local_file = sub_local_file_path
            with open(sub_local_file, 'w') as f:
                f.write(subtitle_string)
        elif choice_index == 2:
            sub_local_file = '{0}/{1}-{2}.srt'.format(dirname, my_filename_sans_ext, lang_of_the_file)
            with open(sub_local_file, 'w') as f:
                f.write(subtitle_string)
        elif choice_index == 3:
            sub_local_file = '{0}/{1}-{2}.srt'.format(dirname, my_filename_sans_ext, now)
            with open(sub_local_file, 'w') as f:
                f.write(subtitle_string)
        elif choice_index == 4:
            sub_local_file = input('Enter a valid file name to save in [{0}]: '.format(dirname))
            with open('{0}/{1}'.format(dirname, sub_local_file), 'w') as f:
                f.write(subtitle_string)
            sub_local_file = '{0}/{1}'.format(dirname, sub_local_file)
        elif choice_index == 5:
            sys.exit('Bye!')
    else:
        overwrite = zenipy.question(title='😎 File exists, overwrite?', text='Answer <b>yes</b> to overwrite \n\n<i>{0}-{1}.srt</i>\n\n Or <b>no</b> to choose the file name.'.format(my_filename_sans_ext, now) + beg, width=330, height=120, timeout=None)

        if overwrite:
            sub_local_file = sub_local_file_path
            with open(sub_local_file, 'w') as f:
                f.write(subtitle_string)
        else:
            sub_local_file = zenipy.file_selection(multiple=False, directory=False, save=True, confirm_overwrite=True, filename=sub_local_file_path, title='Save subtitles file as', width=330, height=120, timeout=None)
            if sub_local_file:
                with open(sub_local_file, 'w') as f:
                    f.write(subtitle_string)
            else:
                sys.exit('Bye!')

        zenipy.message(title='😎 Success', text='File saved to {0}'.format(sub_local_file) + beg, width=330, height=120, timeout=None)

        print('😎 File saved to {0}'.format(sub_local_file))
